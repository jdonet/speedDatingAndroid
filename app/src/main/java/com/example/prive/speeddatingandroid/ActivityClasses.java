package com.example.prive.speeddatingandroid;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.example.prive.speeddatingandroid.metier.Classe;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ActivityClasses extends AppCompatActivity {
    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_classes);

        //récupération du controleur passé en paramètre
       final Controleur controleur = getIntent().getExtras().getParcelable("controleur");
       String filiere= getIntent().getExtras().get("filiere").toString();

        //récupération de la listView
        listView = this.findViewById(R.id.listViewClasses);

        //remplissage de la listView
        controleur.remplirListView(this, listView,classesToListHashmap(controleur.getLesClasses(filiere)));

        //Ajout d'un écouteur quand on clique sur un item. On passe alors à la vue Etudiant en envoyant le controleur et la classe sélectionnée
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //récupération de la classe sur laquelle on a cliqué
                HashMap<String, String> listItem = (HashMap<String, String>) listView.getItemAtPosition(position);
                String classe = listItem.get("champs1").toString();

                Intent intent = new Intent(ActivityClasses.this, ActivityEtudiants.class);
                intent.putExtra("controleur", controleur);
                intent.putExtra("classe",classe);
                finish();
                startActivity(intent);
            }
        });

        Button changerClasse = findViewById(R.id.btRetour);
        changerClasse.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityClasses.this, ActivityFiliere.class);
                intent.putExtra("controleur", controleur);
                finish();
                startActivity(intent);
            }

        });

        Button etudiantsRetenus = findViewById(R.id.btEtudiantsRetenus);
        etudiantsRetenus.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityClasses.this, ActivityEtudiantsRetenus.class);
                intent.putExtra("controleur", controleur);
                finish();
                startActivity(intent);
            }

        });

    }

    /**
     * Prépare les données de la collection pour les mettre dans la listView
     * @param lesClasses : La collection
     * @return : Les données préparées
     */
    private List<HashMap<String, String>> classesToListHashmap(ArrayList<Classe> lesClasses) {
        List<HashMap<String, String>> liste = new ArrayList<HashMap<String, String>>();
        HashMap<String, String> element;

        for (int i = 0; i < lesClasses.size(); i++) {
            element = new HashMap<String, String>();
            element.put("champs1", lesClasses.get(i).getNom());
            element.put("champs2", lesClasses.get(i).getMissions() + "\n" + lesClasses.get(i).getDatesStage());
            liste.add(element);
        }
        return liste;
    }

}
