package com.example.prive.speeddatingandroid.metier;

import android.os.Parcel;
import android.os.Parcelable;

public class Classe  implements Parcelable {
    private String num;
    private String nom;
    private String section;
    private String detailSection;
    private String missions;
    private String datesStage;

    /**
     * Constructeur de la classe Classe
     * @param num
     * @param nom
     * @param section
     * @param detailSection
     * @param missions
     * @param datesStage
     */
    public Classe(String num, String nom, String section, String detailSection, String missions, String datesStage) {
        this.num = num;
        this.nom = nom;
        this.section = section;
        this.detailSection = detailSection;
        this.missions = missions;
        this.datesStage = datesStage;
    }



    public String getNum() {
        return num;
    }

    public String getNom() {
        return nom;
    }

    public String getSection() {
        return section;
    }

    public String getDetailSection() {
        return detailSection;
    }

    public String getMissions() {
        return missions;
    }

    public String getDatesStage() {
        return datesStage;
    }

    ////////////////////////////Methodes des Parcelables ////////////////////
    protected Classe(Parcel in) {
        num = in.readString();
        nom = in.readString();
        section = in.readString();
        detailSection = in.readString();
        missions = in.readString();
        datesStage = in.readString();
    }

    public static final Creator<Classe> CREATOR = new Creator<Classe>() {
        @Override
        public Classe createFromParcel(Parcel in) {
            return new Classe(in);
        }

        @Override
        public Classe[] newArray(int size) {
            return new Classe[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(num);
        dest.writeString(nom);
        dest.writeString(section);
        dest.writeString(detailSection);
        dest.writeString(missions);
        dest.writeString(datesStage);
    }
    ////////////////////////////fin methodes des Parcelables ////////////////////

}
