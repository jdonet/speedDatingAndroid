package com.example.prive.speeddatingandroid;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.app.Dialog;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.example.prive.speeddatingandroid.metier.Etudiant;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ActivityEtudiantsRetenus extends AppCompatActivity {
    private Controleur controleur;
    private ListView listView;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_etudiants_retenus);


        //récupération du controleur et de la classe passés en paramètre
        controleur = getIntent().getExtras().getParcelable("controleur");

        //récupération de la listView
        listView = this.findViewById(R.id.lvEtudiantsRetenus);

        //remplissage de la listView
        controleur.remplirListView(this, listView,classesToListHashmap(controleur.getLesEtudiantsRetenus()));
        context=this;

        Button changerClasse = findViewById(R.id.btRetour);
        changerClasse.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityEtudiantsRetenus.this, ActivityFiliere.class);
                intent.putExtra("controleur", controleur);
                finish();
                startActivity(intent);
            }

        });

        Button btImprimer = findViewById(R.id.btImprimer);
        btImprimer.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context );

                // set title
                alertDialogBuilder.setTitle("Impression liste étudiant");

                // set dialog message
                alertDialogBuilder
                        .setMessage("Voulez-vous imprimer cette liste et quitter l'application ?")
                        .setCancelable(false)
                        .setPositiveButton("Oui",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                //envoie de la liste en WS pour impression
                                controleur.imprimerListe(ActivityEtudiantsRetenus.this);
                                //lancement de la page d'accueil
                                Intent intent = new Intent(ActivityEtudiantsRetenus.this, MainActivity.class);
                                finish();
                                startActivity(intent);

                            }
                        })
                        .setNegativeButton("Non",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                // if this button is clicked, just close
                                // the dialog box and do nothing
                                dialog.cancel();
                            }
                        });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();



            }

        });

        //Ajout d'un écouteur quand on clique sur un item. On passe alors à la vue Etudiant en envoyant le controleur et la classe sélectionnée
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                //récupération de la classe sur laquelle on a cliqué
                HashMap<String, String> listItem = (HashMap<String, String>) listView.getItemAtPosition(position);
               // String classe = listItem.get("champs1").toString();


                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                            context );

                    // set title
                    alertDialogBuilder.setTitle("Suppression étudiant");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("Voulez-vous supprimer cet étudiant de votre liste ?")
                            .setCancelable(false)
                            .setPositiveButton("Oui",new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,int id) {
                                    controleur.getLesEtudiantsRetenus().remove(position);
                                    controleur.remplirListView(ActivityEtudiantsRetenus.this, listView,classesToListHashmap(controleur.getLesEtudiantsRetenus()));
                                }
                            })
                            .setNegativeButton("Non",new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,int id) {
                                    // if this button is clicked, just close
                                    // the dialog box and do nothing
                                    dialog.cancel();
                                }
                            });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();




            }
        });

    }

    /**
     * Prépare les données de la collection pour les mettre dans la listView
     * @param lesEtudiantsRetenus : La collection
     * @return : Les données préparées
     */
    private List<HashMap<String, String>> classesToListHashmap(ArrayList<Etudiant> lesEtudiantsRetenus) {
        List<HashMap<String, String>> liste = new ArrayList<HashMap<String, String>>();
        HashMap<String, String> element;

        for (int i = 0; i < lesEtudiantsRetenus.size(); i++) {
            element = new HashMap<String, String>();
            element.put("champs1", lesEtudiantsRetenus.get(i).getClasse().getNom() + ": "+lesEtudiantsRetenus.get(i).getPrenom() + " "+lesEtudiantsRetenus.get(i).getNom());
            element.put("champs2", lesEtudiantsRetenus.get(i).getDescription());
            liste.add(element);
        }
        return liste;
    }

}
