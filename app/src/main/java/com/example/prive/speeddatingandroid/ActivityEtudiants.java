package com.example.prive.speeddatingandroid;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.example.prive.speeddatingandroid.metier.Etudiant;

import java.util.ArrayList;
import java.util.Collections;

public class ActivityEtudiants extends AppCompatActivity {
    private ListView listView;
    private Controleur controleur;
    private String classeSelectionnee;
    private int numEtudiantActuel;
    private int nbEcartes;
    private int nbConserves;
    ArrayList<Etudiant> lesEtudiants;
//pour le swipe
    private float x1,x2;
    static final int MIN_DISTANCE=150;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_etudiants);

        //récupération du controleur et de la classe passés en paramètre
        controleur = getIntent().getExtras().getParcelable("controleur");
        classeSelectionnee = getIntent().getExtras().getString("classe");
        //on filtre les étudiants par rapport à la classe choisie, et on mélange
        lesEtudiants = filtreEtudiantsClasse(classeSelectionnee,controleur.getLesEtudiants());
        numEtudiantActuel = 0;
        nbEcartes=0;
        nbConserves=controleur.getLesEtudiantsRetenus().size();
        majInfosEtudiant();


        Button changerClasse = findViewById(R.id.chgtClasse);
        changerClasse.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityEtudiants.this, ActivityFiliere.class);
                intent.putExtra("controleur", controleur);
                finish();
                startActivity(intent);
            }

        });

        Button etudiantsRetenus = findViewById(R.id.btEtudiantsRetenus);
        etudiantsRetenus.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityEtudiants.this, ActivityEtudiantsRetenus.class);
                intent.putExtra("controleur", controleur);
                finish();
                startActivity(intent);
            }

        });



    }

    //gestion du swipe
    public boolean onTouchEvent(MotionEvent event){
        switch (event.getAction()){
            case MotionEvent.ACTION_DOWN:
                x1 = event.getX();
                break;
            case  MotionEvent.ACTION_UP:
                x2 = event.getX();
                float deltaX = x1-x2;
                if(deltaX > MIN_DISTANCE){ //swipe gauche, on écarte le profil
                    if(numEtudiantActuel<lesEtudiants.size()) {
                        nbEcartes++;
                        numEtudiantActuel++;
                    }
                    majInfosEtudiant();
                }else if (-deltaX > MIN_DISTANCE){//swipe droit, on garde le profil
                    if(numEtudiantActuel<lesEtudiants.size()) {
                        controleur.getLesEtudiantsRetenus().add(lesEtudiants.get(numEtudiantActuel));
                        nbConserves++;
                        numEtudiantActuel++;
                    }
                    majInfosEtudiant();
                }
                break;
        }
        return super.onTouchEvent(event);
    }



    private ArrayList<Etudiant> filtreEtudiantsClasse(String classeSelectionnee, ArrayList<Etudiant> lesEtudiants){
        ArrayList<Etudiant> liste = new ArrayList<>();
        for (int i = 0; i < lesEtudiants.size(); i++) {
            if (lesEtudiants.get(i).getClasse().getNom().equals(classeSelectionnee)) { //si c'est bien un étudiant de la classe choisie
                boolean trouve=false;
                for (int j = 0; j < controleur.getLesEtudiantsRetenus().size(); j++) {
                    if (controleur.getLesEtudiantsRetenus().get(j).getId().equals(lesEtudiants.get(i).getId())) { //si il est déjà dans la liste des retenus
                        trouve=true;
                    }
                }
                if (!trouve){
                    liste.add(lesEtudiants.get(i));
                }
            }
        }
        //mélanger les éléments de la liste
        Collections.shuffle(liste);
        return liste;
    }

    private void majInfosEtudiant(){

            TextView zoneNom = findViewById(R.id.ztNom);
            TextView zoneMail = findViewById(R.id.ztMail);
            TextView zoneDesc = findViewById(R.id.ztDescription);
            TextView indicateurGauche = findViewById(R.id.leftIndicator);
            TextView indicateurDroit = findViewById(R.id.rightIndicator);
        if(numEtudiantActuel<lesEtudiants.size()) {
            TextView zoneMessage = findViewById(R.id.tvTitre);
            zoneMessage.setText("Liste des étudiants de "+ classeSelectionnee);
            zoneNom.setText((numEtudiantActuel+1) + "/"+lesEtudiants.size()+": "+lesEtudiants.get(numEtudiantActuel).getNom() + " " + lesEtudiants.get(numEtudiantActuel).getPrenom());
            zoneMail.setText(lesEtudiants.get(numEtudiantActuel).getMail());
            zoneDesc.setText(lesEtudiants.get(numEtudiantActuel).getDescription());
            indicateurGauche.setText(nbEcartes + " écartés");
            indicateurDroit.setText(nbConserves + " retenus");
        }else {
            TextView zoneMessage = findViewById(R.id.tvTitre);
            zoneMessage.setText("Plus d'étudiant dans cette classe, merci d'en sélectionner une autre");
            zoneNom.setText("");
            zoneMail.setText("");
            zoneDesc.setText("");
            indicateurGauche.setText(nbEcartes + " écartés");
            indicateurDroit.setText(nbConserves + " retenus");
        }

    }


}