package com.example.prive.speeddatingandroid;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/*Created by btssio on 5/9/18.*/

public class ActivityFiliere extends AppCompatActivity  {

    private TextView choix;
    private Object v;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filiere);

        //récupération du controleur passé en paramètre
        final Controleur controleur = getIntent().getExtras().getParcelable("controleur");

        Button etudiantsRetenus = findViewById(R.id.btListe);
        etudiantsRetenus.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityFiliere.this, ActivityEtudiantsRetenus.class);
                intent.putExtra("controleur", controleur);
                finish();
                startActivity(intent);
            }

        });


        Button btAG = findViewById(R.id.AG);
        btAG.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                    Intent intent = new Intent(ActivityFiliere.this, ActivityClasses.class);
                intent.putExtra("filiere","GPME");
                intent.putExtra("controleur",controleur);
                finish();
                startActivity(intent);
            }

    });



        Button btAM = findViewById(R.id.AM);
        btAM.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityFiliere.this, ActivityClasses.class);
                intent.putExtra("filiere","SAM");
                intent.putExtra("controleur",controleur);
                finish();
                startActivity(intent);
            }

        });

        Button btCG = findViewById(R.id.CG);
        btCG.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityFiliere.this, ActivityClasses.class);
                intent.putExtra("filiere","CG");
                intent.putExtra("controleur",controleur);
                finish();
                startActivity(intent);

            }

        });

        Button btMUC = findViewById(R.id.MUC);
        btMUC.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityFiliere.this, ActivityClasses.class);
                intent.putExtra("filiere","MUC");
                intent.putExtra("controleur",controleur);
                finish();
                startActivity(intent);

            }

        });

        Button btNRC = findViewById(R.id.NRC);
        btNRC.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                Intent SIO = new Intent(ActivityFiliere.this, ActivityClasses.class);
                SIO.putExtra("filiere","NDRC");
                SIO.putExtra("controleur",controleur);
                finish();
                startActivity(SIO);

            }

        });


        Button btSIO = findViewById(R.id.SIO);
        btSIO.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityFiliere.this, ActivityClasses.class);
                intent.putExtra("filiere","SIO");
                intent.putExtra("controleur",controleur);
                finish();
                startActivity(intent);

            }

        });

}
}