package com.example.prive.speeddatingandroid;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private Controleur controleur;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //création du controleur de l'application. On lui indique l'url du WS
        controleur = new Controleur("http://speeddating.donet.yt/ws/");
        //récupération des données
        controleur.recupererData(this);

        //récupération du bouton de l'activity et ouverture de l'activity des filières sur le clic
        final Button connectBtn = (Button) findViewById(R.id.button);
        connectBtn.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View v) {

                //maj nom orga
                EditText zoneSaisieOrga = findViewById(R.id.zoneOrga);
                String organisation = zoneSaisieOrga.getText().toString();

                //modification de l'orga dans le controleur
                controleur.setNomOrganisation(organisation);

                // Confirmation de l'organisation
                if (TextUtils.isEmpty(zoneSaisieOrga.getText())){
                    zoneSaisieOrga.setError("Veuillez remplir le nom de votre organisation");
                }
                else {
                    Intent i = new Intent(getApplicationContext(),ActivityFiliere.class);
                    i.putExtra("controleur",controleur);
                    startActivity(i);
                }
            }

        });

    }

}
