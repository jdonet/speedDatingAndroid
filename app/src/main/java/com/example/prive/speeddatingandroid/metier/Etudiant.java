package com.example.prive.speeddatingandroid.metier;

import android.os.Parcel;
import android.os.Parcelable;

public class Etudiant  implements Parcelable {
    private String id;
    private String nom;
    private String prenom;
    private String mail;
    private String description;
    private Classe classe;

    /**
     * Constructeur de la classe Etudiant
     * @param id
     * @param nom
     * @param prenom
     * @param mail
     * @param description
     * @param classe
     */
    public Etudiant(String id, String nom, String prenom, String mail, String description, Classe classe) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.mail = mail;
        this.description = description;
        this.classe = classe;
    }


    public String getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public String getMail() {
        return mail;
    }

    public String getDescription() {
        return description;
    }

    public Classe getClasse() {
        return classe;
    }

    ////////////////////////////Methodes des Parcelables ////////////////////
    protected Etudiant(Parcel in) {
        id = in.readString();
        nom = in.readString();
        prenom = in.readString();
        mail = in.readString();
        description = in.readString();
        classe = (Classe) in.readValue(Classe.class.getClassLoader());
    }

    public static final Creator<Etudiant> CREATOR = new Creator<Etudiant>() {
        @Override
        public Etudiant createFromParcel(Parcel in) {
            return new Etudiant(in);
        }

        @Override
        public Etudiant[] newArray(int size) {
            return new Etudiant[size];
        }
    };
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(nom);
        dest.writeString(prenom);
        dest.writeString(mail);
        dest.writeString(description);
        dest.writeValue(classe);
    }
    ////////////////////////////fin methodes des Parcelables ////////////////////

}
