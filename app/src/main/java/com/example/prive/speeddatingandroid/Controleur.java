package com.example.prive.speeddatingandroid;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.example.prive.speeddatingandroid.metier.Classe;
import com.example.prive.speeddatingandroid.metier.Etudiant;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * La classe Controleur se charge de faire les acces réseau, la récupération des données,
 * le chargement des collections avec des instances métiers,
 * et l'affichage des données dans les listview
 */
public class Controleur implements Parcelable {
    private ArrayList<Classe> lesClasses = new ArrayList<>();
    private ArrayList<Etudiant> lesEtudiants = new ArrayList<>();
    private ArrayList<Etudiant> lesEtudiantsRetenus = new ArrayList<>();
    private String nomOrganisation;
    private String urlWS;

    /**
     * Constructeur du controleur
     * @param urlWS : L'url de là où est hébergé le WS
     */
    public Controleur(String urlWS) {
        this.urlWS = urlWS;
    }

    public ArrayList<Classe> getLesClasses() {
        return lesClasses;
    }

    //on renvoie les classes filtrées par nom filiere
    public ArrayList<Classe> getLesClasses(String nomFiliere) {
        ArrayList<Classe> collectionTemporaire = new ArrayList<>();

        for (Classe uneClasse: lesClasses) { // pour chaque classe
            if (uneClasse.getSection().equals(nomFiliere)){ //si la classe est de la même filiere que celle recherchée
                collectionTemporaire.add(uneClasse); //on l'ajoute à la collection temporaire
            }
        }

        return collectionTemporaire;
    }

    public ArrayList<Etudiant> getLesEtudiants() {
        return lesEtudiants;
    }

    public ArrayList<Etudiant> getLesEtudiantsRetenus() {
        return lesEtudiantsRetenus;
    }

    public void setNomOrganisation(String nomOrganisation) {
        this.nomOrganisation = nomOrganisation;
    }

    /**
     * A l'initialisation de l'application, récupération de toutes les données, pour ne plus avoir a faire de rechargement sur les autres pages
     * @param activity : L'activity d'Accueil depuis laquelle on récupère les données
     */
    public void recupererData(Activity activity) {
        rechercherEtudiants(activity);
        rechercherClasse(activity);
    }

    /**
     * Appel le WS pour récupérer les classes et charge la collection des classes
     * @param activity
     */
    private void rechercherClasse(Activity activity) {
        new faireRequeteHTTP(activity, urlWS+"ws.php?action=getClasses", "classes").execute();
    }

    /**
     *  Appel le WS pour récupérer les étudiants et charge la collection des étudiants
     * @param activity
     */
    private void rechercherEtudiants(Activity activity) {

        new faireRequeteHTTP(activity, urlWS+"ws.php?action=getEtudiants",  "etudiants").execute();

    }

    /**
     *  Appel le WS pour récupérer les étudiants et charge la collection des étudiants
     * @param activity
     */
    public void imprimerListe(Activity activity) {

        String listeId = "";
        for (int i=0;i<lesEtudiantsRetenus.size();i++) {
            listeId = listeId + lesEtudiantsRetenus.get(i).getId().toString() +";";
        }

        listeId = listeId.substring(0,listeId.length()-1);
        nomOrganisation = nomOrganisation.replace(" ","");
        String url= urlWS+"ws.php?action=imprimerListe&organisation="+nomOrganisation+"&etudiants="+listeId;
        new faireRequeteHTTP(activity, url,  "").execute();

    }


    ////////////////////////////Methodes des Parcelables ////////////////////
    protected Controleur(Parcel in) {

        this.lesClasses = in.readArrayList(getClass().getClassLoader());
        this.lesEtudiants = in.readArrayList(getClass().getClassLoader());
        this.lesEtudiantsRetenus = in.readArrayList(getClass().getClassLoader());
        this.nomOrganisation = in.readString();
        this.urlWS = in.readString();
    }

    public static final Creator<Controleur> CREATOR = new Creator<Controleur>() {
        @Override
        public Controleur createFromParcel(Parcel in) {
            return new Controleur(in);
        }

        @Override
        public Controleur[] newArray(int size) {
            return new Controleur[size];
        }
    };


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(lesClasses);
        dest.writeList(lesEtudiants);
        dest.writeList(lesEtudiantsRetenus);
        dest.writeString(nomOrganisation);
        dest.writeString(urlWS);

    }
    ////////////////////////////fin methodes des Parcelables ////////////////////

    /**
     * Méthode permettant de remplir la listview d'une activity
     * @param activity : L'activity qui contient la listview
     * @param listView : La listView de l'activity
     * @param data : Les données à mettre dans la listView
     */
    public void remplirListView(Activity activity, ListView listView,List<HashMap<String, String>> data) {
        /****************/

        /*
         * On doit donner à notre adaptateur une liste du type « List<Map<String, ?> » :
         *  - la clé doit forcément être une chaîne de caractères
         *  - en revanche, la valeur peut être n'importe quoi, un objet ou un entier par exemple,
         *  si c'est un objet, on affichera son contenu avec la méthode « toString() »
         *
         * Dans notre cas, la valeur sera une chaîne de caractères, puisque le nom et le numéro de téléphone
         * sont entreposés dans des chaînes de caractères
         */

        ListAdapter adapter = new SimpleAdapter(activity.getApplicationContext(),
                //Valeurs à insérer
                data,
                /*
                 * Layout de chaque élément (là, il s'agit d'un layout par défaut
                 * pour avoir deux textes l'un au-dessus de l'autre, c'est pourquoi on
                 * n'affiche que le nom et le numéro d'une personne)
                 */
                android.R.layout.simple_list_item_2,
                /*
                 * Les clés des informations à afficher pour chaque élément :
                 *  - la valeur associée à la clé « text1 » sera la première information
                 *  - la valeur associée à la clé « text2 » sera la seconde information
                 */
                new String[]{"champs1", "champs2"},
                /*
                 * Enfin, les layouts à appliquer à chaque widget de notre élément
                 * (ce sont des layouts fournis par défaut) :
                 *  - la première information appliquera le layout « android.R.id.text1 »
                 *  - la seconde information appliquera le layout « android.R.id.text2 »
                 */
                new int[]{android.R.id.text1, android.R.id.text2});
        //Pour finir, on donne à la ListView le SimpleAdapter
        listView.setAdapter(adapter);

        /*************************/
    }



    /**
     * Classe permettant de faire une requete http asynchrone et d'afficher les résultats dans la listView
     */
    private class faireRequeteHTTP extends AsyncTask<Void, Void, Void> {
        private ProgressDialog pDialog;

        String adresseURL = "";
        String classeAppelante;
        Activity activity;
        boolean erreurAcces=false;

        /**
         * Constructeur de la classe asynchrone faireRequeteHTTP
         * @param activity : L'activity qui lance la requête
         * @param url : L'url à atteindre
         * @param classeAppelante : Le nom de la classe appelante (traitement différent pour remplir les collections)
         */
        public faireRequeteHTTP(Activity activity, String url, String classeAppelante) {
            this.adresseURL = url;
            this.classeAppelante = classeAppelante;
            this.activity = activity;
        }


        /**
         * Appelé avant la requete pour mettre un message d'information
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //Gestion de la progressBar
            pDialog = new ProgressDialog(activity);
            pDialog.setMessage("Getting Data ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();

        }

        /**
         * Lancement de la requete
         * @param args
         * @return
         */
        @Override
        protected Void doInBackground(Void... args) {
            if (reseauOK())
                try {
                    lancerRequete(); //récupération du Json
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("Erreur lecture HTTP", "Erreur dans l'url ou le noms des champs");
                    erreurAcces=true;
                }
            else {
                Log.e("Erreur acces réseau", "Pas de connexion réseau");
                erreurAcces=true;
            }
            return null;
        }

        /**
         * Après l'exécution de la requete, suppression du message d'information
         * @param result
         */
        @Override
        protected void onPostExecute(Void result) {
            pDialog.dismiss();
            if (erreurAcces){
                //message erreur
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        activity );

                // set title
                alertDialogBuilder.setTitle("Erreur acces réseau");

                // set dialog message
                alertDialogBuilder
                        .setMessage("Problème d'accès au service. Vérifiez votre connexion réseau")
                        .setCancelable(false)
                        .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                activity.finish();

                            }
                        });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
            }

        }

        /**
         * Vérifie si le réseau fonctionne
         * @return
         */
        private boolean reseauOK() {
            ConnectivityManager cm = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            if (netInfo != null && netInfo.isConnectedOrConnecting())
                return true;
            else
                return false;
        }

        /**
         * Lance la requête HTTP
         */
        private void lancerRequete() throws Exception {

            InputStream is = null;
            String result = "";


                URL url = new URL(adresseURL);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                is = new BufferedInputStream(urlConnection.getInputStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "utf-8"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();
                result = sb.toString();
                //suivant la classe appelante, on va appeler les méthodes qui remplissent les ArrayList correctement
                switch (classeAppelante) {
                    case "classes":
                        remplirCollectionLesClasses(new JSONArray(result));
                    case "etudiants":
                        remplirCollectionLesEtudiants(new JSONArray(result));
                }


        }

        /**
         * Méthode en charge de parser les données JSON pour alimenter les ArrayList de classes
         * @param json : Les données Json
         */
        private void remplirCollectionLesClasses(JSONArray json) {
            try {
                for (int i = 0; i < json.length(); i++) {
                    JSONObject c = json.getJSONObject(i);
                    //récupération des données de la classe
                    String num = c.getString("num");
                    String nom = c.getString("nom");
                    String section = c.getString("section");
                    String detailSection = c.getString("detailSection");
                    String missions = c.getString("missions");
                    String datesStage = c.getString("datesStage");
                    //création instance de classe
                    Classe classe = new Classe(num, nom, section, detailSection,missions,datesStage);
                    //Ajout de l'instance à la collection
                    lesClasses.add(classe);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        /**
         * Méthode en charge de parser les données JSON pour alimenter les ArrayList d'étudiants
         * @param json : Les données Json
         */
        private void remplirCollectionLesEtudiants(JSONArray json) {
            try {
                for (int i = 0; i < json.length(); i++) {
                    JSONObject c = json.getJSONObject(i);
                    //récupération des données de l'étudiant
                    String id = c.getString("id");
                    String nom = c.getString("nom");
                    String prenom = c.getString("prenom");
                    String mail = c.getString("mail");
                    String description = c.getString("description");
                    //récupération des données de la classe de l'étudiant
                    String num = c.getJSONObject("classe").getString("num");
                    String nomClasse = c.getJSONObject("classe").getString("nom");
                    String section = c.getJSONObject("classe").getString("section");
                    String detailSection = c.getJSONObject("classe").getString("detailSection");
                    String missions = c.getJSONObject("classe").getString("missions");
                    String datesStage = c.getJSONObject("classe").getString("datesStage");
                    //Création de l'instance de classe
                    Classe classe = new Classe(num, nomClasse, section, detailSection,missions,datesStage);
                    //Création de l'instance d'étudiant
                    Etudiant etudiant = new Etudiant(id, nom, prenom, mail, description, classe);
                    //Ajout de l'instance étudiant à la collection
                    lesEtudiants.add(etudiant);

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }
}
